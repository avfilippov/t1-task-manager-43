package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.ITaskService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ITaskDTOService service = new TaskDTOService(connectionService);

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        service.add(USER_TASK1.getUserId(), USER_TASK1);
        service.add(USER_TASK2.getUserId(), USER_TASK2);
        service.add(ADMIN_TASK1.getUserId(), ADMIN_TASK1);
    }

    @After
    public void after() throws UserIdEmptyException {
        service.clear(USER1.getId());
    }

    @Test
    public void add() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER_TASK2.getUserId(), USER_TASK2));
        Assert.assertThrows(Exception.class, () -> service.add(NULL_TASK.getUserId(), NULL_TASK));
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER1.getId(), USER_TASK2));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(ADMIN_TASK1.getUserId(), USER2.getId());
    }

    @Test
    public void updateByNullId() {
        Assert.assertNull(service.findOneById(USER1.getId(), null));
    }


}
