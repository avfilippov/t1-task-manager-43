package ru.t1.avfilippov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm.tm_session")
public final class SessionDTO extends AbstractUserOwnedModel {

    @NotNull
    @Column
    private Date date = new Date();

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
