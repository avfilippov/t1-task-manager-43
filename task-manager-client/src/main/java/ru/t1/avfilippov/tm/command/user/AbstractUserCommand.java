package ru.t1.avfilippov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.avfilippov.tm.api.endpoint.IUserEndpoint;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.exception.entity.UserNotFoundException;
import ru.t1.avfilippov.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }


    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
