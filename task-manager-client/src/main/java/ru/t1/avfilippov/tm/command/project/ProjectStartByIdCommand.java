package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "start project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setProjectId(id);
        getProjectEndpoint().startProjectById(request);
    }

}
